import React from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import {getPosts,getPostsById} from '../../redux/actions/posts/postAction'
import { Link } from 'react-router-dom';
class Article extends React.PureComponent{
  constructor(props){
    super(props);
    this.state={

    }
  }

  componentDidMount(){
    this.props.getPosts();
  }


  render(){
    return(
      <div>

      <Link  to={`/article/${ this.props.data._id}`}>
          {
            this.props.data.map((data,index)=>{
            return <p key={index}>{data.title} : {data.description}</p>
          })
          }
      </Link>

      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return{
    data : state.postReducer.data
  }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    getPosts,
    getPostsById
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(Article)