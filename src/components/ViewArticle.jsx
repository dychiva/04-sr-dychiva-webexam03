import React from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import {getPosts,getPostsById} from '../../redux/actions/posts/postAction'
import { Link } from 'react-router-dom';
class ViewArticle extends React.PureComponent{
  constructor(props){
    super(props);
    this.state={

    }
  }

  componentDidMount(){
    this.props.getPosts();
  }


  render(){
    return(
      <div>
            <div className="col-sm-8" style={{marginRight:"0"}}>
            <h1> {this.state.data.title}</h1>
            <p>{this.state.data.description}</p>
            <Link to="/">
                <p>See more...</p>
            </Link>
          
          </div>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return{
    data : state.postReducer.data,
    id : state.postReducer.data._id
  }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    getPosts,
    getPostsById
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewArticle)