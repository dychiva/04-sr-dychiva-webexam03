import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Articles from './components/Articles';
import ViewArticle from './components/ViewArticle';

function App() {
  return (
    <div className="App">
      <Router>
       <Switch>
          <Route exact path="/" component={Articles}/>
          <Route  path="/aricle/:id" component={ViewArticle}/>
       </Switch>
      </Router>
       
    </div>
  );
}

export default App;
