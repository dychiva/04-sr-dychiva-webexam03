import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware} from 'redux';
import {rootReducer} from './redux/reducers/rootReducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger/src'
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

const middlewares = [thunk, logger]
const store = createStore(rootReducer, applyMiddleware(...middlewares));
ReactDOM.render (
    <Provider store = {store}>
        <BrowserRouter>
            <React.StrictMode>
                <App/>
            </React.StrictMode>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
serviceWorker.unregister();
