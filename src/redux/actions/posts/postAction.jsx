import {GET_POSTS,GETID_POSTS} from './ArticleActionType';
import Axios from 'axios';

export const getPosts = () =>{
    console.log("Get aticle");
    
    const innnerGetPosts = async (dispatch) =>{
        const result = await Axios.get("https://jsonplaceholder.typicode.com/posts")
        dispatch({
            type : GET_POSTS,
            data:result.data
        })
        
    }
    return innnerGetPosts
}

export const getPostsById = (id) =>{
    console.log("Get by id aticle");
    
    const innnerGetPosts = async (dispatch) =>{
        const result = await Axios.get("http://110.74.194.125:3535/api/articles/"+id)
        dispatch({
            type : GETID_POSTS,
            data:result.data,
            id: result.data._id
        })
        
    }
    return innnerGetPosts
}

