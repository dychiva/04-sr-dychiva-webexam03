import { GET_POSTS, GETID_POSTS } from "../actions/posts/ArticleActionType"

const defautlState = {
    data :[]
}
export const articleReducer = (state  = defautlState,action) =>{
    switch(action.type){
        case GET_POSTS :
            return{
                ...state,
                data : action.data
            }
        case GETID_POSTS :
            return{
                ...state,
                data: action.data
            }
        default :
            return state;
    }
}